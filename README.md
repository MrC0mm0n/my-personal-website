# README #

Under Construction

### What is this repository for? ###

* This is a repo-collection of my work
* The working application of this repo is available in the link [http://mohd-rawoof.cfapps.io/](http://mohd-rawoof.cfapps.io/)

### How do I get set up? ###

Under construction

Issue #1, documents the steps to setup the project. (Under Construction)

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Technologies ###

* [Intel Edison](http://www.intel.com/content/www/us/en/do-it-yourself/edison.html)
* [Spring 4](http://spring.io/)
* [Hibernate 4](http://hibernate.org/)
* [Pivotal Web Services](https://run.pivotal.io/)
* [IBM Bluemix](http://www-01.ibm.com/software/bluemix/)
* [ClearDB](https://www.cleardb.com/)
* [Bootstrap](http://getbootstrap.com/)
* [ChartJS](http://www.chartjs.org/)
* [D3-JS](http://d3js.org/)
* [Twilio](https://www.twilio.com/)
* [Google API](https://code.google.com/apis/console)
* [GMail API](https://developers.google.com/gmail/)
* [Google Charts](https://developers.google.com/chart/)
* [Youtube API](https://developers.google.com/youtube/)
* [Twitter API](https://dev.twitter.com/rest/public)
* [Facebook API](https://developers.facebook.com/)
* [Wikipedia API](http://www.mediawiki.org/wiki/API:Main_page)

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Documentation ###

* Issue #2, documents the 'Video Search' application
* Issue #3, documents the 'Sensor Web' application

### License ###

* This code/application is under the GNU General Public License

### Who do I talk to? ###

* The admin of this repo can be reached at 'inform.smohdr88@gmail.com'