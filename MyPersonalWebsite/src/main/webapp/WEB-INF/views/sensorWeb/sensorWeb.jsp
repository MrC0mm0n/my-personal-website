<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page session="false"%>

<!DOCTYPE html>
<html>
<head>
<title>Home</title>
<!-- Adding favicon -->
<link rel="icon" href="resources/images/Logo.ico">

<!-- Latest compiled and minified Bootstrap CSS -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">

<!-- Optional Bootstrap theme -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css">

<link rel="stylesheet" href="resources/chartsjs/legend.css">

</head>
<body>
	<div class="container">
		<h1>Sensor Web, built at the UofT Hack!</h1>
		<p>Site accessed on ${serverTime}. This site helps show live
			sensor data.</p>
		<h4>
			Send an SMS to <b>(647) 559-3979</b> with your <b>Email ID</b> to
			receive a report in your email.
		</h4>

		<div id="server-down" class="row">
			<div class="col-md-7">
				<div class="alert alert-danger" role="alert">
					<strong>Oh Snap!</strong> Looks like the server is down.
				</div>
			</div>
		</div>

		<div id="device-offline" class="row">
			<div class="col-md-7">
				<div class="alert alert-warning" role="alert">
					<strong>Oh Gosh!</strong> Looks like the device is offline.
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-7">
				<canvas id="temperature-line-graph" width="640" height="480"></canvas>
			</div>

			<div class="col-md-2">
				<div id="temperature-line-graph-legend"></div>
				<div>&nbsp;</div>
				<div id="live-data"></div>
			</div>

			<div class="col-md-3">
				<p>Powered by,</p>
				<ul>
					<li><a
						href="http://www.intel.com/content/www/us/en/do-it-yourself/edison.html"
						target="_blank">Intel Edison</a></li>
					<li><a href="http://spring.io/" target="_blank">Spring 4</a></li>
					<li><a href="http://hibernate.org/" target="_blank">Hibernate
							4</a></li>
					<li><a href="https://run.pivotal.io/" target="_blank">Pivotal
							Web Services</a></li>
					<li><a href="http://www-01.ibm.com/software/bluemix/"
						target="_blank">IBM Bluemix</a></li>
					<li><a href="https://www.cleardb.com/" target="_blank">ClearDB</a></li>
					<li><a href="http://getbootstrap.com/" target="_blank">Bootstrap</a></li>
					<li><a href="http://www.chartjs.org/" target="_blank">ChartJS</a></li>
					<li><a href="https://www.twilio.com/" target="_blank">Twilio</a></li>
					<li><a href="https://developers.google.com/gmail/"
						target="_blank">GMail API</a></li>
					<li><a href="https://developers.google.com/chart/"
						target="_blank">Google Charts</a></li>
				</ul>
			</div>
		</div>
	</div>

	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>

	<!-- Latest compiled and minified Bootstrap JavaScript -->
	<script
		src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

	<script src="resources/chartsjs/Chart.min.js"></script>
	<script src="resources/chartsjs/legend.js"></script>

	<script src="resources/js/sensorWeb/sensorWeb.js"></script>

</body>
</html>
