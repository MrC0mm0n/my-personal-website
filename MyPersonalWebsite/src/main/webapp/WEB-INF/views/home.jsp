<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page session="false"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Home</title>
<!-- Adding favicon -->
<link rel="icon" href="resources/images/Logo.ico">

<!-- Latest compiled and minified Bootstrap CSS -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">

<!-- Optional Bootstrap theme -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css">

<!-- JQuery UI CSS -->
<link rel="stylesheet"
	href="resources/jquery-ui-1.11.2/jquery-ui.min.css">
<link rel="stylesheet"
	href="resources/jquery-ui-1.11.2/jquery-ui.structure.min.css">
<link rel="stylesheet"
	href="resources/jquery-ui-1.11.2/jquery-ui.theme.min.css">

<!-- Custom CSS for page -->
<link rel="stylesheet" href="resources/css/home.css">

</head>
<body>

	<div class="site-wrapper">
		<div class="site-wrapper-inner">
			<div class="cover-container">

				<div class="masthead clearfix">
					<div class="inner">
						<h3 class="masthead-brand">
							<a href="<spring:message
											code="nav1.link" />"><spring:message
									code="logo.text" /></a>
						</h3>
						<nav>
							<ul class="nav masthead-nav">
								<li class="active"><a
									href="<spring:message
											code="nav1.link" />"><spring:message
											code="nav1" /></a></li>
								<li><a
									href="<spring:message
											code="nav2.link" />"><spring:message
											code="nav2" /></a></li>
								<li><a href="<spring:message code="nav3.link" />"
									target="_blank"><spring:message code="nav3" /></a></li>
							</ul>
						</nav>
					</div>
				</div>

				<div class="inner cover">
					<div>
						<img class="img-circle" src="resources/images/Me.jpg"
							alt="Mohammed Rawoof" style="width: 35%; height: 35%;">
					</div>
					<div>
						<br>
					</div>
					<div>
						<p class="lead">Hi!... The guy in specs and a tie is Me...</p>
						<p class="lead">'Me' meaning 'Mohammed Rawoof'...</p>
						<p class="lead">
							I guess I'll leave <a href="https://www.facebook.com/MrC0mm0n"
								target="_blank"> <img alt="facebook-icon" height="3%"
								width="3%" src="resources/images/facebook-icon.png" />
							</a> and <a href="https://ca.linkedin.com/in/mohammedrawoofshaik"
								target="_blank"> <img alt="linkedin-icon" height="3%"
								width="3%" src="resources/images/linkedin-icon.png" />
							</a> to describe me better...
						</p>
					</div>
				</div>

				<div class="mastfoot">
					<div class="inner">
						<p></p>
					</div>
				</div>

			</div>
		</div>
	</div>

	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<!-- JQuery UI library -->
	<script type="text/javascript"
		src="resources/jquery-ui-1.11.2/jquery-ui.min.js"></script>
	<!-- Latest compiled and minified Bootstrap JavaScript -->
	<script
		src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

	<!-- Code to run in page -->
	<script type="text/javascript" src="resources/js/home.js"></script>
</body>
</html>
