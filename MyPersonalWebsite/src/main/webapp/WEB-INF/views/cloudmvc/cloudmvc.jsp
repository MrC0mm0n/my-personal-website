<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page session="false"%>

<!doctype html>
<html>
<head>

<!-- Adding favicon -->
<link rel="icon" href="resources/images/Logo.ico">

<!-- Latest compiled and minified Bootstrap CSS -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">

<!-- Optional Bootstrap theme -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css">

<!-- Bootstrap Validator theme -->
<link rel="stylesheet"
	href="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/css/bootstrapValidator.min.css" />

<!-- Dynatable theme -->
<!-- <link rel="stylesheet" media="all"
	href="https://s3.amazonaws.com/dynatable-docs-assets/css/jquery.dynatable.css"> -->

<title>Home</title>
</head>
<body>

	<div class="container">

		<h4 align="center" class="page-header">Customer Collection System</h4>

		<form:form action="save" modelAttribute="customerInfo">

			<div class="row">
				<div class="col-md-4">
					<div class="input-group">
						<span class="input-group-addon">ID #</span>
						<form:hidden path="id" />
						<form:input path="customer_id" class="form-control" />
					</div>
				</div>
				<div class="col-md-4">
					<div class="input-group">
						<span class="input-group-addon">Name</span>
						<form:input path="customer_name" class="form-control" />
					</div>
				</div>
				<div class="col-md-4">
					<div class="input-group">
						<span class="input-group-addon">Address</span>
						<form:input path="customer_address" class="form-control" />
					</div>
				</div>
			</div>

			<div class="row" align="center">&nbsp;</div>

			<div class="row" align="center">
				<input type="submit" value="Save" class="btn btn-default" />
			</div>

		</form:form>

		<div class="row">
			<div class="col-md-1"></div>
			<div class="col-md-10">
				<h4 align="center" class="page-header">Customers in database
					DB1</h4>
				<table class="table table-striped">
					<thead>
						<tr>
							<th>ID #</th>
							<th>Name</th>
							<th>Address</th>
							<th>Actions</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${customerList}" var="list">
							<tr>
								<td><c:out value="${list.customer_id}"></c:out></td>
								<td><c:out value="${list.customer_name}"></c:out></td>
								<td><c:out value="${list.customer_address}"></c:out></td>
								<td><a href="update?id=${list.id}"><span
										class="glyphicon glyphicon-edit"></span></a> <a
									href="delete?id=${list.id}"><span
										class="glyphicon glyphicon-trash"></span></a></td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
			<div class="col-md-1"></div>
		</div>

		<div class="row">
			<div class="col-md-1">
				<form:form action="syncdb2">
					<input type="submit" value="Sync DB2" class="btn btn-default" />
				</form:form>
			</div>
			<div class="col-md-10">
				<h4 align="center" class="page-header">Customers in database
					DB2</h4>
				<table class="table table-striped">
					<thead>
						<tr>
							<th>ID #</th>
							<th>Name</th>
							<th>Address</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${customerListDB2}" var="list">
							<tr>
								<td><c:out value="${list.customer_id}"></c:out></td>
								<td><c:out value="${list.customer_name}"></c:out></td>
								<td><c:out value="${list.customer_address}"></c:out></td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
			<div class="col-md-1"></div>
		</div>

		<div class="row">
			<div class="col-md-2">
				<input type="button" id="btn-ajax" value="Ajax Call"
					class="btn btn-default" />
			</div>
			<div class="col-md-2">
				<form:form action="asyncMethod">
					<input type="submit" value="Call Async method"
						class="btn btn-default" />
				</form:form>
			</div>
			<div class="col-md-8"></div>
		</div>

	</div>

	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<!-- Latest compiled and minified Bootstrap JavaScript -->
	<script
		src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

	<!-- Bootstrap Validator JS -->
	<script type="text/javascript"
		src="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/js/bootstrapValidator.min.js"></script>

	<!-- Dynatable library -->
	<script type="text/javascript"
		src="https://s3.amazonaws.com/dynatable-docs-assets/js/jquery.dynatable.js"></script>

	<script type="text/javascript" src="../resources/js/cloudmvc/cloudmvc.js"></script>
</body>
</html>