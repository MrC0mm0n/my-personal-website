<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page session="false"%>

<!DOCTYPE html>
<html>
<head>
<title>GWS-Project X</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="icon" href="../resources/images/Logo.ico">

<!-- Latest compiled and minified Bootstrap CSS -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">

<!-- Optional Bootstrap theme -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css">

</head>

<body>

	<div class="container">

		<div class="row" align="center">
			<h4>Welcome!</h4>
			<p>What can I search, for you?</p>
		</div>

		<div class="row">

			<div class="col-md-3"></div>
			<div class="col-md-6">
				<form:form modelAttribute="searchModel" autocomplete="off">
					<div class="input-group">
						<form:input path="searchData" type="text" class="form-control" />
						<span class="input-group-btn">
							<button class="btn btn-default" type="submit">Go!</button>
						</span>
					</div>
				</form:form>
			</div>
			<div class="col-md-3">
				<form:form method="get">
					<div class="input-group">
						<button class="btn btn-default" type="submit">Reset</button>
					</div>
				</form:form>
			</div>

		</div>

		<div class="row" style="border-bottom: medium;">&nbsp;</div>

		<c:if test="${not empty searchModel.searchResults}">
			<table class="table">
				<thead>
					<tr>
						<th>Youtube</th>
						<th>Wikipedia</th>
						<th>Twitter</th>
						<th>Facebook</th>
						<th>Yahoo</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${searchModel.searchResults}" var="i">
						<tr>
							<td><p id="<c:out value="${i.videoID}"></c:out>">
									<a href="#" data-toggle="modal" data-target="#basicModal"><c:out
											value="${i.videoTitle}"></c:out></a>
								</p>
								<p>
									<img alt="video image"
										src="<c:out value="${i.videoImgURL}"></c:out>">
								</p></td>
							<td id="wiki-<c:out value="${i.videoID}"></c:out>"></td>
							<td id="twitter-<c:out value="${i.videoID}"></c:out>"></td>
							<td></td>
							<td></td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</c:if>

		<!-- Modal -->
		<div class="modal fade bs-example-modal-lg" id="basicModal"
			data-backdrop="static">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal"
							aria-hidden="true">
							<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
						</button>
						<h4 class="modal-title" id="modalTitle">Modal title</h4>
					</div>
					<div class="modal-body" id="videoModal" align="center"></div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" id="btnCloseModal"
							data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>


	</div>

	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>

	<!-- Latest compiled and minified Bootstrap JavaScript -->
	<script
		src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

	<script type="text/javascript"
		src="../resources/js/videoSearch/videoSearch.js"></script>
		
</body>
</html>
