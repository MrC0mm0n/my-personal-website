<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page session="false"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Visual Home page</title>
<!-- Adding favicon -->
<link rel="icon" href="resources/images/Logo.ico">

<!-- Latest compiled and minified Bootstrap CSS -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">

<!-- Optional Bootstrap theme -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css">

</head>
<body>

	<canvas id="sitemap"></canvas>

	<div class="container">
		<p>
			Powered by <a href="http://arborjs.org/" target="_blank">ArborJS</a>
		</p>
	</div>

	<!-- jQuery library -->
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>

	<!-- Arbour JS library -->
	<script src="resources/arbor/arbor.js"></script>
	<script src="resources/arbor/arbor-tween.js"></script>
	<script src="resources/arbor/arbor-graphics.js"></script>

	<!-- Latest compiled and minified Bootstrap JavaScript -->
	<script
		src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

	<!-- Code to run in page -->
	<script type="text/javascript" src="resources/js/visualWeb/visualWeb.js"></script>

</body>
</html>