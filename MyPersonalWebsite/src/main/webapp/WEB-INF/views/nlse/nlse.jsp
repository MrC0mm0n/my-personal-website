<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page session="false"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Home</title>
<!-- ADD BEGIN: 19/OCT/2014: Mohammed R -->
<!-- Adding favicon -->
<link rel="icon" href="../resources/images/Logo.ico">
<!-- ADD END -->
<!-- Latest compiled and minified Bootstrap CSS -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">

<!-- Optional Bootstrap theme -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css">

<!-- Bootstrap Validator theme -->
<link rel="stylesheet"
	href="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/css/bootstrapValidator.min.css" />

<!-- JQuery UI CSS -->
<link rel="stylesheet"
	href="../resources/jquery-ui-1.11.2/jquery-ui.min.css">
<link rel="stylesheet"
	href="../resources/jquery-ui-1.11.2/jquery-ui.structure.min.css">
<link rel="stylesheet"
	href="../resources/jquery-ui-1.11.2/jquery-ui.theme.min.css">

<!-- Dynatable theme -->
<link rel="stylesheet" media="all"
	href="https://s3.amazonaws.com/dynatable-docs-assets/css/jquery.dynatable.css">

</head>
<body>
	<div class="container">

		<!-- <div class="page-header" align="center">
			<h1>Open DB Integration</h1>
		</div> -->
		<!-- <div class="row">&nbsp;</div> -->

		<%-- <jsp:include page="navigation.jsp"></jsp:include> --%>

		<div class="row" align="center">

			<h1>Welcome!</h1>
			<p>What can I answer, for you?</p>

		</div>

		<div class="row">
			<div class="col-md-3"></div>
			<div class="col-md-6">
				<form:form modelAttribute="searchModel" autocomplete="off">
					<div class="input-group">
						<form:input path="searchData" type="text" class="form-control" />
						<span class="input-group-btn">
							<button class="btn btn-default" type="submit">Go!</button>
						</span>
					</div>
				</form:form>
			</div>
			<div class="col-md-3">
				<form:form method="get">
					<div class="input-group">
						<button class="btn btn-default" type="submit">Reset</button>
					</div>
				</form:form>
			</div>
		</div>

		<div class="row" style="border-bottom: medium;">&nbsp;</div>

		<div class="row">
			<div class="col-md-1"></div>
			<div class="col-md-10">
				<c:if test="${not empty searchModel.searchData}">
					<table class="table" id="result-table">
						<thead style="color: white">
							<tr>
								<th>Type</th>
								<th>Title</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${searchResults}" var="i" varStatus="iCount">
								<c:forEach items="${i.value}" var="j" varStatus="jCount">
									<c:forEach items="${j.value}" var="k" varStatus="kCount">
										<c:if test="${j.key != 'url'}">
											<tr>
												<td><span class="label label-info">${i.key}</span></td>
												<td><a href="${j.key}${k}" target="_blank">${k}</a></td>
											</tr>
										</c:if>
									</c:forEach>
								</c:forEach>
							</c:forEach>
						</tbody>
					</table>
				</c:if>
			</div>
			<div class="col-md-1"></div>
		</div>

		<div class="row">&nbsp;</div>
		<div class="row">
			<div class="col-md-2"></div>
			<div class="col-md-8"></div>
			<div class="col-md-2">
				<c:if test="${not empty searchModel.searchData}">
					<p>
						<a href="#" data-target="#analysisModal" data-toggle="modal">Analysis</a>
					</p>
				</c:if>
			</div>
		</div>

		<!-- Modal -->
		<div class="modal fade" id="analysisModal" role="dialog"
			aria-labelledby="myLargeModalLabel" aria-hidden="true"
			data-backdrop="static" data-keyboard="false">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">
							<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
						</button>
						<h4 class="modal-title" id="myModalLabel">Analysis</h4>
					</div>
					<div class="modal-body">
						<div class="row" style="padding-left: 15px; padding-right: 15px;">
							<div>
								<b>Query Keywords:</b>
								<p>
									<c:forEach items="${searchQuery}" var="i" varStatus="iCount">
										<c:out value="${i}"></c:out>
										<c:if test="${not iCount.last}">,</c:if>
									</c:forEach>
								</p>
							</div>
							<div>
								<b>Query URLs:</b>
								<p>
									<c:forEach items="${searchResults}" var="i" varStatus="iCount">
										<c:forEach items="${i.value}" var="j" varStatus="jCount">
											<c:forEach items="${j.value}" var="k" varStatus="kCount">
												<c:if test="${j.key eq 'url'}">
													<p>
														<a href="${k}" target="_blank">${k}</a>
													</p>
												</c:if>
											</c:forEach>
										</c:forEach>
									</c:forEach>
								</p>
							</div>
							<div>
								<b>Execution Time:</b>
								<p>${execTime}sec.</p>
							</div>
							<div>
								<b>API:</b>
								<table class="table">
									<thead>
										<tr>
											<th>#</th>
											<th>Source</th>
											<th>Sandbox</th>
											<th>API</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>1</td>
											<td>Wikipedia</td>
											<td><a
												href="http://en.wikipedia.org/wiki/Special:ApiSandbox"
												target="_blank">Wikipedia-Sandbox</a></td>
											<td><a href="http://en.wikipedia.org/w/api.php"
												target="_blank">Wikipedia-API</a></td>
										</tr>
										<tr>
											<td>2</td>
											<td>MediaWiki</td>
											<td><a
												href="http://www.mediawiki.org/wiki/Special:ApiSandbox"
												target="_blank">MediaWiki-Sandbox</a></td>
											<td><a href="http://www.mediawiki.org/w/api.php"
												target="_blank">MediaWiki-API</a></td>
										</tr>
										<tr>
											<td>3</td>
											<td>Semantic MediaWiki</td>
											<td><a
												href="http://semantic-mediawiki.org/wiki/Special:Ask"
												target="_blank">Semantic MediaWiki-Sandbox</a></td>
											<td><a href="http://semantic-mediawiki.org/w/api.php"
												target="_blank">Semantic MediaWiki-API</a></td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>

	</div>

	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<!-- JQuery UI library -->
	<script type="text/javascript"
		src="../resources/jquery-ui-1.11.2/jquery-ui.min.js"></script>
	<!-- Latest compiled and minified Bootstrap JavaScript -->
	<script
		src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

	<!-- Bootstrap Validator JS -->
	<script type="text/javascript"
		src="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/js/bootstrapValidator.min.js"></script>

	<!-- Dynatable library -->
	<script type="text/javascript"
		src="https://s3.amazonaws.com/dynatable-docs-assets/js/jquery.dynatable.js"></script>

	<!-- ChartJS -->
	<!-- Source code: https://github.com/nnnick/Chart.js -->

	<!-- D3 JS -->
	<script src="http://d3js.org/d3.v3.min.js" charset="utf-8"></script>

	<!-- Code to run in page -->
	<script type="text/javascript" src="../resources/js/nlse/nlse.js"></script>
</body>
</html>
