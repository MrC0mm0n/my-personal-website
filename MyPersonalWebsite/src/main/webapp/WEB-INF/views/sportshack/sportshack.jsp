<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page session="false"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title>RU SportsHack</title>

<link rel="icon"
	type="resources/images/sportshack/favicon-ice-hockey.ico"
	href="favicon.ico" />

<!-- Getting bootstrap config -->
<!-- Bootstrap compiled and minified CSS -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">
<!-- Optional Bootstrap theme -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap-theme.min.css">
<!-- Dynatable theme -->
<link rel="stylesheet" media="all"
	href="https://s3.amazonaws.com/dynatable-docs-assets/css/jquery.dynatable.css">

<!-- Custom style sheet -->
<link rel="stylesheet" href="resources/css/sportshack/sportshack.css">

</head>
<body>

	<div id="ice-player-1">
		<img src="resources/images/sportshack/IceHocPlayer-icon.png">
	</div>

	<div class="container">
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>
		<div class="row">&nbsp;</div>

		<!-- <div class="row">
			<a class="btn btn-default" href="WorstHockey.html">Demo Tracking
				(Worst case scenario)</a>
		</div> -->

		<div class="row">&nbsp;</div>

		<!-- Monitoring co-ordinates of players -->
		<table class="table" id="player-monitor-table">
			<caption>Player Monitor</caption>
			<thead>
				<tr>
					<th>#</th>
					<th>Player</th>
					<th>Co-ordinates</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>1</td>
					<td>Mark</td>
					<td><p id="coord-player-1"></p></td>
				</tr>
				<tr>
					<td>2</td>
					<td>Jacob</td>
					<td></td>
				</tr>
			</tbody>
		</table>

	</div>

	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script type="text/javascript"
		src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<!-- Bootstrap compiled and minified JavaScript -->
	<script type="text/javascript"
		src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
	<!-- Dynatable library -->
	<script type="text/javascript"
		src="https://s3.amazonaws.com/dynatable-docs-assets/js/jquery.dynatable.js"></script>
	<!-- Socket IO -->
	<script src="https://cdn.socket.io/socket.io-1.2.0.js"></script>

	<!-- Custom JS code -->
	<script type="text/javascript" src="resources/js/sportshack/sportshack.js"></script>

</body>
</html>