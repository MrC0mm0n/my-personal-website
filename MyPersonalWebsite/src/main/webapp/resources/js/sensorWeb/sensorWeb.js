$(document).ready(
		function() {
			$('#server-down').hide();
			$('#device-offline').hide();

			// Data for the chart
			var data = {
				labels : [ "YYYY-MM-DD HH:MM:SS.s", "YYYY-MM-DD HH:MM:SS.s",
						"YYYY-MM-DD HH:MM:SS.s", "YYYY-MM-DD HH:MM:SS.s",
						"YYYY-MM-DD HH:MM:SS.s", "YYYY-MM-DD HH:MM:SS.s",
						"YYYY-MM-DD HH:MM:SS.s", "YYYY-MM-DD HH:MM:SS.s",
						"YYYY-MM-DD HH:MM:SS.s", "YYYY-MM-DD HH:MM:SS.s" ],
				datasets : [ {
					label : "Temperature(C)",
					fillColor : "rgba(220,220,220,0.2)",
					strokeColor : "rgba(220,220,220,1)",
					pointColor : "rgba(220,220,220,1)",
					pointStrokeColor : "#fff",
					pointHighlightFill : "#fff",
					pointHighlightStroke : "rgba(220,220,220,1)",
					data : [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ]
				}, {
					label : "Solar",
					fillColor : "rgba(151,187,205,0.2)",
					strokeColor : "rgba(151,187,205,1)",
					pointColor : "rgba(151,187,205,1)",
					pointStrokeColor : "#fff",
					pointHighlightFill : "#fff",
					pointHighlightStroke : "rgba(151,187,205,1)",
					data : [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ]
				}, {
					label : "Sound Pollution",
					fillColor : "rgba(172,230,170,0.2)",
					strokeColor : "rgba(172,230,170,1)",
					pointColor : "rgba(172,230,170,1)",
					pointStrokeColor : "#fff",
					pointHighlightFill : "#fff",
					pointHighlightStroke : "rgba(172,230,170,1)",
					data : [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ]
				}, {
					label : "Air Pressure",
					fillColor : "rgba(242,197,138,0.2)",
					strokeColor : "rgba(242,197,138,1)",
					pointColor : "rgba(242,197,138,1)",
					pointStrokeColor : "#fff",
					pointHighlightFill : "#fff",
					pointHighlightStroke : "rgba(242,197,138,1)",
					data : [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ]
				} ]
			}

			// Update dataset for chart
			var updateDataTemperature = function(oldData, sensorData) {
				var labels = oldData["labels"];
				labels.shift();
				labels.push(sensorData[0]);

				var dataSetA = oldData["datasets"][0]["data"];
				dataSetA.push(sensorData[1]);
				dataSetA.shift();

				var dataSetB = oldData["datasets"][1]["data"];
				dataSetB.push(sensorData[2]);
				dataSetB.shift();

				var dataSetC = oldData["datasets"][2]["data"];
				dataSetC.push(sensorData[3]);
				dataSetC.shift();

				var dataSetD = oldData["datasets"][3]["data"];
				dataSetD.push(sensorData[4]);
				dataSetD.shift();
			};

			// Setting up the chart
			var temperatureChart = new Chart(document.getElementById(
					"temperature-line-graph").getContext("2d"));
			temperatureChart.Line(data, {
				datasetFill : false
			});
			legend(document.getElementById("temperature-line-graph-legend"),
					data);

			// Code to get new readings from the server
			var lastDate = '';
			setInterval(function() {
				$.ajax({
					url : "sensorWeb/plot",
					success : function(sensorData) {
						// console.log(sensorData);
						sensorData = sensorData.split(',');

						/*
						 * updateDataTemperature(data, sensorData);
						 * temperatureChart.Line(data, { animation : false,
						 * datasetFill : false });
						 */

						if (lastDate != sensorData[0]) {
							updateDataTemperature(data, sensorData);
							temperatureChart.Line(data, {
								animation : false,
								datasetFill : false
							});
							lastDate = sensorData[0]
							
							$('#live-data').empty();
							$('#live-data').html("<p>Time: " + sensorData[0] + "</p><p>Temp: " + sensorData[1] + "</p><p>Solar: " + sensorData[2] + "</p><p>Sound: " + sensorData[3] + "</p><p>Pressure: " + sensorData[4] + "</p>");

							$('#device-offline').hide();
							$('#server-down').hide();
						} else {
							$('#device-offline').show();
							$('#server-down').hide();
						}
					},
					error : function() {
						$('#server-down').show();
						$('#device-offline').hide();
					}
				});
			}, 3300);

		});