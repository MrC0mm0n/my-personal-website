$(document).ready(function() {

	$('form').bootstrapValidator({        
        fields: {
        	searchData: {
                validators: {
                    notEmpty: {
                        message: 'Daaaa... You need to ask a question!'
                    }
                }
            }
        }
    });
	
	// ADD BEGIN: 19/OCT/2014: Mohammed R
	// Making results table searchable, by invoking dynatable API
	/*$('#result-table').find('tr').each(function (index, value) {        
		var td = String($(this).find('td').get(0));
        var data = td.split('+');
        console.log(data[0]);
    });*/
	$('#result-table').dynatable();
	// ADD END

});