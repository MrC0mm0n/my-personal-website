$(document).ready(function() {

	$('p').click(function() {
		// console.log($($(this).children()[0]).text());

		var videoTitle = $($(this).children()[0]).text();
		var videoId = $(this).attr('id');
		
		$('#modalTitle').empty();
		$('#videoModal').empty();
		$('#wiki-' + videoId).empty();
		$('#twitter-' + videoId).empty();

		$('#modalTitle').text(videoTitle);
		$('#videoModal').html("<iframe id='ytplayer' type='text/html' width='720' height='405' src='https://www.youtube.com/embed/" + videoId + "' frameborder='0' allowfullscreen>");
		
		$.ajax({
			url : 'searchWiki',
			data : videoTitle,
			type : 'POST'
		}).done(function(data) {
			// console.log(data);
			if(data != ''){	
				var htmlData = '';
				var arrData = data.split(',');
				for(i in arrData){
					htmlData = "<p><a href='http://en.wikipedia.org/wiki/" + arrData[i] + "' target='_blank'>" + arrData[i] + "</p>" + htmlData;
				}
				$('#wiki-' + videoId).html(htmlData);
			} else {
				$('#wiki-' + videoId).html("<p>No relevant wiki content found.</p>");
			}
		});
		
		$.ajax({
			url : 'searchTwitter',
			data : videoTitle,
			type : 'POST'
		}).done(function(data) {
			// console.log(data);
			if(data != ''){	
				var htmlData = '';
				var arrData = data.split(',');
				for(i in arrData){
					htmlData = htmlData + "<p><a href='https://twitter.com/@" + arrData[i] + "' target='_blank'>" + arrData[i] + '</a></p>';
				}
				htmlData = htmlData + '</p>';
				$('#twitter-' + videoId).html(htmlData);
			} else {
				$('#twitter-' + videoId).html("<p>No users sharing this video found.</p>");
			}
		});

	});

	$('#btnCloseModal').click(function() {
		$('#videoModal').empty();
	})

});