package my.personal.website;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Handles requests for the application home page.
 */
@Controller
public class HomeController {
	private static final Logger logger = LoggerFactory
			.getLogger(HomeController.class);

	/**
	 * Simply selects the home view to render by returning its name.
	 */
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home() {
		logger.info("Entering HomeController.home()");

		logger.info("Exiting HomeController.home()");
		return "home";
	}

	@RequestMapping(value = "/about", method = RequestMethod.GET)
	public String about() {
		logger.info("Entering HomeController.about()");

		logger.info("Exiting HomeController.about()");
		return "home";
	}

	@RequestMapping(value = "/portfolio", method = RequestMethod.GET)
	public String portfolio() {
		logger.info("Entering HomeController.portfolio()");

		logger.info("Exiting HomeController.portfolio()");
		return "portfolio";
	}

	@RequestMapping(value = "/visualWeb", method = RequestMethod.GET)
	public String visualWeb() {
		logger.info("Entering HomeController.visualWeb()");

		logger.info("Exiting HomeController.visualWeb()");
		return "visualWeb/visualWeb";
	}

	@RequestMapping(value = "/sportsHack")
	public String sportsHack() {
		logger.info("Entering HomeController.sportsHack()");

		logger.info("Exiting HomeController.sportsHack()");
		return "sportshack/sportshack";
	}

}
