package my.personal.website.NLSE.SearchEngine;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.HashSet;
import java.util.concurrent.RecursiveTask;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class SearchByText extends
		RecursiveTask<HashMap<String, HashSet<String>>> {
	private static final Logger logger = LoggerFactory
			.getLogger(SearchByText.class);

	private static final long serialVersionUID = 1L;
	private String searchData;
	int searchLimit;

	public SearchByText(String searchData, int searchLimit) {
		this.searchData = searchData;
		this.searchLimit = searchLimit;
	}

	protected HashMap<String, HashSet<String>> compute() {
		long T0 = System.currentTimeMillis();

		HashMap<String, HashSet<String>> data = new HashMap<String, HashSet<String>>();
		HashSet<String> store = new HashSet<String>();

		String wiki = "http://en.wikipedia.org/w/api.php" + "?";
		// String mediaWiki = "http://www.mediawiki.org/w/api.php";
		// String semanticWiki = "http://semantic-mediawiki.org/w/api.php";
		String action = wiki + "action=query" + "&";
		String list = action + "list=search" + "&";
		String format = list + "format=xml" + "&";
		String limit = format + "srlimit=" + searchLimit + "&";
		String url = limit + "srsearch=" + searchData.replaceAll(" ", "%20");

		URL URLResult;
		DocumentBuilder dBuilder;
		Document doc;
		NodeList allList = null;
		try {
			URLResult = new URL(url);
			dBuilder = DocumentBuilderFactory.newInstance()
					.newDocumentBuilder();
			doc = dBuilder.parse(URLResult.openStream());
			doc.getDocumentElement().normalize();
			allList = doc.getElementsByTagName("query");
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		String wikiLink = "http://en.wikipedia.org/wiki/";
		int httpCode = 0;
		HttpURLConnection connection;
		for (int i = 0; i < allList.getLength(); i++) {
			Node nNode = allList.item(i);
			if (nNode.hasChildNodes()) {
				NodeList itemList = nNode.getChildNodes();
				for (int j = 0; j < itemList.getLength(); j++) {
					Node item = itemList.item(j);
					if (item.hasChildNodes()) {
						NodeList subNodeList = item.getChildNodes();
						for (int k = 0; k < subNodeList.getLength(); k++) {
							Node subNode = subNodeList.item(k);
							NamedNodeMap attrList = subNode.getAttributes();
							String link = "";
							for (int l = 0; l < attrList.getLength(); l++) {
								Node attr = attrList.item(l);
								if (attr.getNodeName().equals("title")) {
									link = attr.getNodeValue();
									try {
										connection = (HttpURLConnection) new URL(
												wikiLink
														+ link.replace(" ", "_"))
												.openConnection();

										connection.connect();
										httpCode = connection.getResponseCode();
										connection.disconnect();
									} catch (IOException e) {
										e.printStackTrace();
									}
									if (httpCode == 200) {
										store.add(link);
									}
								}
							}
						}
					}
				}
			}
		}

		data.put(wikiLink, store);
		HashSet<String> hsbuff = new HashSet<String>();
		hsbuff.add(url);
		data.put("url", hsbuff);

		long Tend = System.currentTimeMillis();
		long Tdiff = (Tend - T0) / 1000;
		logger.info("Time required by SearchByText(" + searchData + ") - "
				+ Tdiff + " sec.");

		return data;
	}
}
