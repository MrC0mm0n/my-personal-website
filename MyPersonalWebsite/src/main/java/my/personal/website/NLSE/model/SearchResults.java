package my.personal.website.NLSE.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

@Entity
@Table(name = "SEARCHRESULTS")
public class SearchResults {

	@Autowired
	private SessionFactory sessionFactory;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID")
	private Integer ID;

	@Column(name = "URL")
	private String URL;

	public SearchResults() {

	}

	public Integer getID() {
		return ID;
	}

	public void setID(Integer iD) {
		ID = iD;
	}

	public String getURL() {
		return URL;
	}

	public void setURL(String uRL) {
		URL = uRL;

		sessionFactory.getCurrentSession().saveOrUpdate(uRL);
	}

}
