package my.personal.website.NLSE;

import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;

import javax.xml.parsers.ParserConfigurationException;

import my.personal.website.NLSE.SearchEngine.SearchEngine;
import my.personal.website.NLSE.SearchEngine.Stopwords;
import my.personal.website.NLSE.model.SearchModel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.xml.sax.SAXException;

// NLSE stands for Natural Language Search Engine
@Controller
@RequestMapping("/nlse")
public class NLSEController {
	private static final Logger logger = LoggerFactory
			.getLogger(NLSEController.class);
	ModelAndView search;
	SearchModel searchModel;

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public ModelAndView home() {
		logger.info("Entering NLSEController.home()");

		searchModel = new SearchModel();

		search = new ModelAndView("nlse/nlse", "searchModel", searchModel);

		logger.info("Exiting NLSEController.home()");
		return search;
	}

	@RequestMapping(value = "/", method = RequestMethod.POST)
	public ModelAndView search(ModelAndView search, SearchModel searchModel)
			throws IOException, ParserConfigurationException, SAXException {
		logger.info("Entering NLSEController.search().POST");

		Stopwords stpwords = new Stopwords();

		HashSet<String> searchQuery = stpwords.removeStopWords(searchModel
				.getSearchData());
		HashMap<String, HashMap<String, HashSet<String>>> searchResults = new HashMap<String, HashMap<String, HashSet<String>>>();

		long startTime = System.currentTimeMillis();
		SearchEngine srchEng = new SearchEngine();
		searchResults = srchEng.search(searchQuery);
		long stopTime = System.currentTimeMillis();
		long elapsedTime = (stopTime - startTime) / 1000;
		/* storeToDB(searchResults); */

		search.addObject("searchQuery", searchQuery);
		search.addObject("searchResults", searchResults);
		search.addObject("searchModel", searchModel);
		search.addObject("execTime", elapsedTime);

		search.setViewName("nlse/nlse");

		logger.info("Exiting NLSEController.search().POST");
		return search;
	}

}
