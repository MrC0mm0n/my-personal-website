package my.personal.website.NLSE.model;

import java.util.HashMap;

public class SearchModel {
	String searchData;
	String searchType;
	HashMap<String, String> listType;
	String searchFormat;
	HashMap<String, Integer> listFormat;

	public SearchModel() {
		// Setting default search criterias
		setSearchType("Text Search");
		setSearchFormat("XML");

		// Providing a list of search criterias - type
		listType = new HashMap<String, String>();
		listType.put("Categories", "Enumerate all categories");
		listType.put("Text Search",
				"Perform a full text search. Limited to 50 pages from Wikipedia's end");

		// Providing a list of search criterias - return type format
		listFormat = new HashMap<String, Integer>();
		listFormat.put("XML", 1);
		listFormat.put("json", 0);
	}

	public String getSearchType() {
		return searchType;
	}

	public void setSearchType(String searchType) {
		this.searchType = searchType;
	}

	public String getSearchFormat() {
		return searchFormat;
	}

	public void setSearchFormat(String searchFormat) {
		this.searchFormat = searchFormat;
	}

	public String getSearchData() {
		return searchData;
	}

	public void setSearchData(String searchData) {
		this.searchData = searchData;
	}

	public HashMap<String, String> getListType() {
		return listType;
	}

	public void setListType(HashMap<String, String> listType) {
		this.listType = listType;
	}

	public HashMap<String, Integer> getListFormat() {
		return listFormat;
	}

	public void setListFormat(HashMap<String, Integer> listFormat) {
		this.listFormat = listFormat;
	}

}
