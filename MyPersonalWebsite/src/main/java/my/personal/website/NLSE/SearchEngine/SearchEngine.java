package my.personal.website.NLSE.SearchEngine;

import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.ForkJoinPool;

import javax.xml.parsers.ParserConfigurationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

public class SearchEngine {

	private static final Logger logger = LoggerFactory
			.getLogger(SearchEngine.class);

	HashMap<String, HashSet<String>> data;
	HashSet<String> store;
	int searchLimit = 50;

	public HashMap<String, HashMap<String, HashSet<String>>> search(
			HashSet<String> searchQuery) throws IOException,
			ParserConfigurationException, SAXException {
		logger.info("Entering SearchEngine.search()");

		long startTime = System.currentTimeMillis();

		Iterator<String> iterQuery = searchQuery.iterator();

		// ADD BEGIN: 06/NOV/2014: Mohammed R
		// Search on multiple words parallely
		ForkJoinPool pool = new ForkJoinPool();

		SearchWord[] queryWords = new SearchWord[searchQuery.size()];
		int i = 0;
		while (iterQuery.hasNext()) {
			queryWords[i] = new SearchWord(iterQuery.next(), searchLimit);
			pool.execute(queryWords[i]);
			i++;
		}

		HashMap<String, HashMap<String, HashSet<String>>> searchResults = new HashMap<String, HashMap<String, HashSet<String>>>();
		String sKey = "";
		for (int j = 0; j < queryWords.length; j++) {
			HashMap<String, HashMap<String, HashSet<String>>> dump = new HashMap<String, HashMap<String, HashSet<String>>>();
			dump = queryWords[j].join();
			Set<String> keys = dump.keySet();
			Iterator<String> iterKey = keys.iterator();
			while (iterKey.hasNext()) {
				sKey = iterKey.next();
				searchResults.put(sKey, dump.get(sKey));
			}
		}

		pool.shutdown();
		// ADD END

		long stopTime = System.currentTimeMillis();
		long elapsedTime = (stopTime - startTime) / 1000;
		logger.info("Time required - By Search Engine - " + elapsedTime
				+ " sec.");

		logger.info("Exiting SearchEngine.search()");
		return searchResults;
	}

}
