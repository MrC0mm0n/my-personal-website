package my.personal.website.NLSE.SearchEngine;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.HashSet;
import java.util.concurrent.RecursiveTask;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class SearchByPages extends
		RecursiveTask<HashMap<String, HashSet<String>>> {
	private static final Logger logger = LoggerFactory
			.getLogger(SearchByPages.class);

	private static final long serialVersionUID = 1L;
	private String searchData;
	int searchLimit;

	public SearchByPages(String searchData, int searchLimit) {
		this.searchData = searchData;
		this.searchLimit = searchLimit;
	}

	protected HashMap<String, HashSet<String>> compute() {
		long T0 = System.currentTimeMillis();

		HashMap<String, HashSet<String>> data = new HashMap<String, HashSet<String>>();
		HashSet<String> store = new HashSet<String>();

		String wiki = "http://en.wikipedia.org/w/api.php" + "?";
		// String mediaWiki = "http://www.mediawiki.org/w/api.php";
		// String semanticWiki = "http://semantic-mediawiki.org/w/api.php";
		String action = wiki + "action=query" + "&";
		String list = action + "list=allpages" + "&";
		String format = list + "format=xml" + "&";
		String limit = format + "aplimit=" + searchLimit + "&";
		String url = limit + "apfrom=" + searchData.replaceAll(" ", "%20");

		URL URLResult;
		DocumentBuilder dBuilder;
		Document doc;
		NodeList allCategList = null;
		try {
			URLResult = new URL(url);
			dBuilder = DocumentBuilderFactory.newInstance()
					.newDocumentBuilder();
			doc = dBuilder.parse(URLResult.openStream());
			doc.getDocumentElement().normalize();
			allCategList = doc.getElementsByTagName("allpages");
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		String wikiLink = "http://en.wikipedia.org/wiki/";
		String link = "";
		int httpCode = 0;
		HttpURLConnection connection;
		for (int i = 0; i < allCategList.getLength(); i++) {
			Node nNode = allCategList.item(i);
			if (nNode.hasChildNodes()) {
				NodeList categList = nNode.getChildNodes();
				for (int j = 0; j < categList.getLength(); j++) {
					Node categ = categList.item(j);
					NamedNodeMap attrList = categ.getAttributes();
					for (int l = 0; l < attrList.getLength(); l++) {
						Node attr = attrList.item(l);
						if (attr.getNodeName().equals("title")) {
							link = attr.getNodeValue();
							try {
								connection = (HttpURLConnection) new URL(
										wikiLink + link.replace(" ", "_"))
										.openConnection();

								connection.connect();
								httpCode = connection.getResponseCode();
								connection.disconnect();
							} catch (IOException e) {
								e.printStackTrace();
							}
							if (httpCode == 200) {
								store.add(link);
							}
						}
					}
				}
			}
		}

		data.put(wikiLink, store);
		HashSet<String> hsbuff = new HashSet<String>();
		hsbuff.add(url);
		data.put("url", hsbuff);

		long Tend = System.currentTimeMillis();
		long Tdiff = (Tend - T0) / 1000;
		logger.info("Time required by SearchByPages(" + searchData + ") - "
				+ Tdiff + " sec.");

		return data;
	}
}
