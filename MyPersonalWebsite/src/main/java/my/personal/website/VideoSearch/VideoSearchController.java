package my.personal.website.VideoSearch;

import java.io.IOException;
import java.util.concurrent.ForkJoinPool;

import javax.xml.parsers.ParserConfigurationException;

import my.personal.website.VideoSearch.SearchEngine.SearchTwitter;
import my.personal.website.VideoSearch.SearchEngine.SearchWikiByText;
import my.personal.website.VideoSearch.SearchEngine.YoutubeSearch;
import my.personal.website.VideoSearch.util.SearchModel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.xml.sax.SAXException;

@Controller
@RequestMapping("/videoSearch")
public class VideoSearchController {

	private static final Logger logger = LoggerFactory
			.getLogger(VideoSearchController.class);

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String homeGet(Model model) {
		logger.info("Enterring VideoSearchController.home().GET");

		model.addAttribute("searchModel", new SearchModel());

		logger.info("Exitting VideoSearchController.home().GET");
		return "videoSearch/videoSearch";
	}

	@Value("${youtube.apiKey}")
	String youtubeApikey;

	@Value("${youtube.video.maxlimit}")
	long videoLimit;

	@RequestMapping(value = "/", method = RequestMethod.POST)
	public String homePost(Model model, SearchModel searchModel) {
		logger.info("Enterring VideoSearchController.home().POST");

		YoutubeSearch youtubeSearch = new YoutubeSearch(youtubeApikey,
				videoLimit);
		searchModel.setSearchResults(youtubeSearch.search(searchModel
				.getSearchData()));

		logger.info("Exitting VideoSearchController.home().POST");
		return "videoSearch/videoSearch";
	}

	@RequestMapping(value = "/searchWiki", method = RequestMethod.POST)
	public @ResponseBody String searchWiki(@RequestBody String searchData)
			throws IOException, ParserConfigurationException, SAXException {
		logger.info("Enterring VideoSearchController.searchWiki()");

		int searchLimit = 5;

		ForkJoinPool pool = new ForkJoinPool();

		SearchWikiByText byWikiText = new SearchWikiByText(searchData,
				searchLimit);

		pool.execute(byWikiText);
		String wikiLinks = byWikiText.join();

		pool.shutdown();

		logger.info("Exitting VideoSearchController.searchWiki()");
		return wikiLinks;
	}

	@Value("${twitter.consumerkey}")
	String twitterConsumerkey;

	@Value("${twitter.secretkey}")
	String twitterSecretkey;

	@Value("${twitter.accesstoken}")
	String twitterAccesstoken;

	@Value("${twitter.accesstokensecret}")
	String twitterAccesstokenSecret;

	@Value("${twitter.user.maxlimit}")
	int searchLimit;

	@RequestMapping(value = "/searchTwitter", method = RequestMethod.POST)
	public @ResponseBody String searchTwitter(@RequestBody String searchData)
			throws IOException, ParserConfigurationException, SAXException {
		logger.info("Enterring VideoSearchController.searchTwitter()");

		ForkJoinPool pool = new ForkJoinPool();

		SearchTwitter byTwiiter = new SearchTwitter(searchData, searchLimit,
				twitterConsumerkey, twitterSecretkey, twitterAccesstoken,
				twitterAccesstokenSecret);

		pool.execute(byTwiiter);
		String twitterUsers = byTwiiter.join();

		pool.shutdown();

		logger.info("Exitting VideoSearchController.searchTwitter()");
		return twitterUsers;
	}

}
