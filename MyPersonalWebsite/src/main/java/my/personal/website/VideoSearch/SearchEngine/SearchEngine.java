package my.personal.website.VideoSearch.SearchEngine;

import java.io.IOException;
import java.util.HashSet;
import java.util.concurrent.ForkJoinPool;

import javax.xml.parsers.ParserConfigurationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

public class SearchEngine {

	private static final Logger logger = LoggerFactory
			.getLogger(SearchEngine.class);

	HashSet<String> store;

	public static String search(String searchData, int searchLimit,
			String twitterConsumerkey, String twitterSecretkey,
			String twitterAccesstoken, String twitterAccesstokenSecret)
			throws IOException, ParserConfigurationException, SAXException {
		logger.info("Entering SearchEngine.search()");

		ForkJoinPool pool = new ForkJoinPool();

		SearchWikiByText byText = new SearchWikiByText(searchData, searchLimit);
		SearchTwitter inTwitter = new SearchTwitter(searchData, searchLimit,
				twitterConsumerkey, twitterSecretkey, twitterAccesstoken,
				twitterAccesstokenSecret);

		pool.execute(byText);
		pool.execute(inTwitter);

		String data = byText.join();

		pool.shutdown();

		logger.info("Exiting SearchEngine.search()");
		return data;
	}
}
