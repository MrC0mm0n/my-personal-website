package my.personal.website.VideoSearch.SearchEngine;

import java.util.List;
import java.util.concurrent.RecursiveTask;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import twitter4j.Query;
import twitter4j.QueryResult;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.conf.ConfigurationBuilder;

public class SearchTwitter extends RecursiveTask<String> {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static final Logger logger = LoggerFactory
			.getLogger(SearchTwitter.class);

	String TWITTER_CONSUMER_KEY;
	String TWITTER_SECRET_KEY;
	String TWITTER_ACCESS_TOKEN;
	String TWITTER_ACCESS_TOKEN_SECRET;

	private String searchData;
	int searchLimit;

	public SearchTwitter(String searchData, int searchLimit,
			String twitterConsumerkey, String twitterSecretkey,
			String twitterAccesstoken, String twitterAccesstokenSecret) {
		this.searchData = searchData;
		this.searchLimit = searchLimit;
		TWITTER_CONSUMER_KEY = twitterConsumerkey;
		TWITTER_SECRET_KEY = twitterSecretkey;
		TWITTER_ACCESS_TOKEN = twitterAccesstoken;
		TWITTER_ACCESS_TOKEN_SECRET = twitterAccesstokenSecret;
	}

	protected String compute() {
		logger.info("Entering SearchTwitter.compute()");

		ConfigurationBuilder cb = new ConfigurationBuilder();
		cb.setDebugEnabled(true).setOAuthConsumerKey(TWITTER_CONSUMER_KEY)
				.setOAuthConsumerSecret(TWITTER_SECRET_KEY)
				.setOAuthAccessToken(TWITTER_ACCESS_TOKEN)
				.setOAuthAccessTokenSecret(TWITTER_ACCESS_TOKEN_SECRET);

		Twitter twitter = new TwitterFactory(cb.build()).getInstance();
		String data = "";
		try {
			Query query = new Query(searchData).count(searchLimit);
			QueryResult result;
			// do {
			// twitter.setOAuthAccessToken(new AccessToken(""));
			// twitter.setOAuthAccessToken("a2hhbmFhaG1lZDEwQGdtYWlsLmNvbTptYWlsNDUy");
			result = twitter.search(query);
			List<Status> tweets = result.getTweets();
			// System.out.println("size of tweets: " + tweets.size());
			for (Status tweet : tweets) {
				/*
				 * logger.info("@" + tweet.getUser().getScreenName() + " - " +
				 * tweet.getText());
				 */
				data = data + "," + tweet.getUser().getScreenName();
			}
			if (data.length() > 0) {
				data = data.substring(1);
			}
		} catch (TwitterException te) {
			logger.info("Failed to search tweets: " + te.getMessage());
		}// end catch

		logger.info("Exiting SearchTwitter.compute()");
		return data;
	}// end function
}