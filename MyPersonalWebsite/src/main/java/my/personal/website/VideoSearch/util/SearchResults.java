package my.personal.website.VideoSearch.util;

public class SearchResults {
	String videoID;
	String videoTitle;
	String videoImgURL;
	String wikipediaURL;

	public String getVideoID() {
		return videoID;
	}

	public void setVideoID(String videoID) {
		this.videoID = videoID;
	}

	public String getVideoTitle() {
		return videoTitle;
	}

	public void setVideoTitle(String videoTitle) {
		this.videoTitle = videoTitle;
	}

	public String getVideoImgURL() {
		return videoImgURL;
	}

	public void setVideoImgURL(String videoImgURL) {
		this.videoImgURL = videoImgURL;
	}

	public String getWikipediaURL() {
		return wikipediaURL;
	}

	public void setWikipediaURL(String wikipediaURL) {
		this.wikipediaURL = wikipediaURL;
	}
}
