package my.personal.website.VideoSearch.util;

import java.util.List;

public class SearchModel {
	String searchData;
	List<SearchResults> searchResults;

	public String getSearchData() {
		return searchData;
	}

	public void setSearchData(String searchData) {
		this.searchData = searchData;
	}

	public List<SearchResults> getSearchResults() {
		return searchResults;
	}

	public void setSearchResults(List<SearchResults> searchResults) {
		this.searchResults = searchResults;
	}

}
