package my.personal.website.VideoSearch.SearchEngine;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import my.personal.website.VideoSearch.util.SearchResults;

import com.google.api.client.googleapis.json.GoogleJsonResponseException;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestInitializer;
import com.google.api.services.youtube.YouTube;
import com.google.api.services.youtube.model.ResourceId;
import com.google.api.services.youtube.model.SearchListResponse;
import com.google.api.services.youtube.model.SearchResult;
import com.google.api.services.youtube.model.Thumbnail;

public class YoutubeSearch {

	String youtubeApikey;
	long videoLimit;

	public YoutubeSearch(String apikey, long limit) {
		youtubeApikey = apikey;
		videoLimit = limit;
	}

	public List<SearchResults> search(String queryTerm) {
		List<SearchResult> searchResultList = new ArrayList<SearchResult>();
		List<SearchResults> listSearchResults = new ArrayList<SearchResults>();
		YouTube youtube;
		try {
			youtube = new YouTube.Builder(Auth.HTTP_TRANSPORT,
					Auth.JSON_FACTORY, new HttpRequestInitializer() {
						public void initialize(HttpRequest request)
								throws IOException {
						}
					}).setApplicationName("youtube-cmdline-search-sample")
					.build();

			YouTube.Search.List search = youtube.search().list("id,snippet");

			/*
			 * Set your developer key from the Google Developers Console for
			 * non-authenticated requests. See:
			 * https://console.developers.google.com/
			 */			
			search.setKey(youtubeApikey);
			search.setQ(queryTerm);

			/*
			 * Restrict the search results to only include videos. See:
			 * https://developers.google.com/youtube/v3/docs/search/list#type
			 */search.setType("video");

			/*
			 * To increase efficiency, only retrieve the fields that the
			 * application uses.
			 */
			search.setFields("items(id/kind,id/videoId,snippet/title,snippet/thumbnails/default/url)");
			search.setMaxResults(videoLimit);

			// Call the API and print results.
			SearchListResponse searchResponse = search.execute();
			searchResultList = searchResponse.getItems();

			if (searchResultList != null) {
				listSearchResults = prettyPrint(searchResultList.iterator(),
						queryTerm);
			}

		} catch (GoogleJsonResponseException e) {
			System.err.println("There was a service error: "
					+ e.getDetails().getCode() + " : "
					+ e.getDetails().getMessage());
		} catch (IOException e) {
			System.err.println("There was an IO error: " + e.getCause() + " : "
					+ e.getMessage());
		} catch (Throwable t) {
			t.printStackTrace();
		}

		return listSearchResults;

	}

	/*
	 * Prints out all results in the Iterator. For each result, print the title,
	 * video ID, and thumbnail.
	 * 
	 * @param iteratorSearchResults Iterator of SearchResults to print
	 * 
	 * @param query Search query (String)
	 */
	private static List<SearchResults> prettyPrint(
			Iterator<SearchResult> iteratorSearchResults, String query) {

		/*
		 * System.out .println(
		 * "\n=============================================================");
		 * System.out.println("   First " + NUMBER_OF_VIDEOS_RETURNED +
		 * " videos for search on \"" + query + "\"."); System.out .println(
		 * "=============================================================\n");
		 */

		if (!iteratorSearchResults.hasNext()) {
			System.out.println(" There aren't any results for your query.");
		}

		List<SearchResults> listSearchResults = new ArrayList<SearchResults>();
		while (iteratorSearchResults.hasNext()) {

			SearchResult singleVideo = iteratorSearchResults.next();
			ResourceId rId = singleVideo.getId();

			// Confirm that the result represents a video. Otherwise, the
			// item will not contain a video ID.
			if (rId.getKind().equals("youtube#video")) {
				Thumbnail thumbnail = singleVideo.getSnippet().getThumbnails()
						.getDefault();

				/*
				 * System.out.println(" Video Id" + rId.getVideoId());
				 * System.out.println(" Title: " +
				 * singleVideo.getSnippet().getTitle());
				 * System.out.println(" Thumbnail: " + thumbnail.getUrl());
				 * System.out .println(
				 * "\n-------------------------------------------------------------\n"
				 * );
				 */

				SearchResults searchResult = new SearchResults();
				searchResult.setVideoID(rId.getVideoId());
				searchResult.setVideoTitle(singleVideo.getSnippet().getTitle());
				searchResult.setVideoImgURL(thumbnail.getUrl());
				listSearchResults.add(searchResult);
			}
		}

		return listSearchResults;
	}
}
