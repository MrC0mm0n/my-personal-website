package my.personal.website.SensorWeb;

import static com.googlecode.charts4j.Color.SKYBLUE;

import java.io.UnsupportedEncodingException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Properties;

import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import my.personal.website.SensorWeb.dao.MeasurementDAO;
import my.personal.website.SensorWeb.model.Measurement;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.googlecode.charts4j.AxisLabels;
import com.googlecode.charts4j.AxisLabelsFactory;
import com.googlecode.charts4j.AxisStyle;
import com.googlecode.charts4j.AxisTextAlignment;
import com.googlecode.charts4j.Color;
import com.googlecode.charts4j.Data;
import com.googlecode.charts4j.GCharts;
import com.googlecode.charts4j.Line;
import com.googlecode.charts4j.LineChart;
import com.googlecode.charts4j.LineStyle;
import com.googlecode.charts4j.Plots;
import com.googlecode.charts4j.Shape;
import com.twilio.sdk.verbs.Message;
import com.twilio.sdk.verbs.TwiMLException;
import com.twilio.sdk.verbs.TwiMLResponse;

@Controller
public class SensorWebController {

	private static final Logger logger = LoggerFactory
			.getLogger(SensorWebController.class);

	@Autowired
	public MeasurementDAO measurementDAO;

	/**
	 * Simply selects the home view to render by returning its name.
	 */
	@RequestMapping(value = "sensorWeb", method = RequestMethod.GET)
	public String home(Locale locale, Model model) {
		logger.info("Entering SensorWebController.home()");

		Date date = new Date();
		DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG,
				DateFormat.LONG, locale);

		String formattedDate = dateFormat.format(date);
		model.addAttribute("serverTime", formattedDate);

		logger.info("Exiting SensorWebController.home()");
		return "sensorWeb/sensorWeb";
	}

	@Value("${sensorweb.gmail.username}")
	String fromUser;

	@Value("${sensorweb.gmail.password}")
	String fromPass;

	@RequestMapping(value = "sensorWeb/report", method = RequestMethod.GET, produces = "application/xml")
	public @ResponseBody String report(@RequestParam("Body") String toUser) {

		logger.info("Entering SensorWebController.report()");

		// Chart Code
		LineChart chart = createChart();

		// JavaMail GMail via SSL
		Properties props = new Properties();
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.socketFactory.port", "465");
		props.put("mail.smtp.socketFactory.class",
				"javax.net.ssl.SSLSocketFactory");
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.port", "465");

		Session session = Session.getDefaultInstance(props,
				new javax.mail.Authenticator() {
					protected PasswordAuthentication getPasswordAuthentication() {
						return new PasswordAuthentication(fromUser, fromPass);
					}
				});

		try {
			javax.mail.Message messageSend = new MimeMessage(session);
			messageSend.setFrom(new InternetAddress(fromUser));
			messageSend.setSubject("Weather report " + new Date());
			messageSend.addRecipient(javax.mail.Message.RecipientType.TO,
					new InternetAddress(toUser));

			Multipart multiPartMessageSend = new MimeMultipart();

			String data = "Hi,\n\nBelow this a link, on clicking "
					+ "which will open in a new tab/window to display the report.\n\n"
					+ "Do not reply to this email. This is an auto-generated email.\n\n";
			MimeBodyPart txtPart = new MimeBodyPart();
			txtPart.setContent(data, "text/plain");
			multiPartMessageSend.addBodyPart(txtPart);

			MimeBodyPart htmlPart = new MimeBodyPart();
			htmlPart.setContent("<img src=\"" + chart.toURLForHTML() + "\">",
					"text/html");
			multiPartMessageSend.addBodyPart(htmlPart);

			messageSend.setContent(multiPartMessageSend);

			Transport.send(messageSend);
		} catch (MessagingException msgExp) {
			logger.error("Something went wrong sending the email");
			msgExp.printStackTrace();
		}

		// Twilio
		TwiMLResponse twiml = new TwiMLResponse();
		Message messageTwil = new Message(
				"The weather report has been sent to '" + toUser + "' email.");
		try {
			twiml.append(messageTwil);
		} catch (TwiMLException e) {
			logger.error("Something went wrong sending the SMS");
			e.printStackTrace();
		}

		logger.info("Exiting SensorWebController.report()");

		return twiml.toXML();
	}

	private LineChart createChart() {

		// Defining lines
		final int NUM_POINTS = 25;
		final double[] dataA = new double[NUM_POINTS];
		final double[] dataB = new double[NUM_POINTS];
		final double[] dataC = new double[NUM_POINTS];
		final double[] dataD = new double[NUM_POINTS];

		for (int i = 0; i < NUM_POINTS; i++) {
			dataA[i] = 100 - (Math.cos(30 * i * Math.PI / 180) * 10 + 50) * i
					/ 20;
			dataB[i] = (Math.cos(30 * i * Math.PI / 180) * 10 + 50) * i / 20;
			dataC[i] = 100 - (Math.sin(30 * i * Math.PI / 180) * 10 + 50) * i
					/ 20;
			dataD[i] = (Math.sin(30 * i * Math.PI / 180) * 10 + 50) * i / 20;
		}

		Line line1 = Plots.newLine(Data.newData(dataA), Color.LIGHTGREY,
				"Temperature(C)");
		line1.setLineStyle(LineStyle.newLineStyle(1, 1, 0));
		line1.addShapeMarkers(Shape.CIRCLE, Color.LIGHTGREY, 5);

		Line line2 = Plots.newLine(Data.newData(dataB), SKYBLUE, "Solar");
		line2.setLineStyle(LineStyle.newLineStyle(1, 1, 0));
		line2.addShapeMarkers(Shape.CIRCLE, SKYBLUE, 5);

		Line line3 = Plots.newLine(Data.newData(dataC), Color.LIGHTGREEN,
				"Sound Pollution");
		line3.setLineStyle(LineStyle.newLineStyle(1, 1, 0));
		line3.addShapeMarkers(Shape.CIRCLE, Color.LIGHTGREEN, 5);

		Line line4 = Plots.newLine(Data.newData(dataD), Color.ORANGE,
				"Air Pressure");
		line4.setLineStyle(LineStyle.newLineStyle(1, 1, 0));
		line4.addShapeMarkers(Shape.CIRCLE, Color.ORANGE, 5);

		// Defining chart.
		LineChart chart = GCharts.newLineChart(line1, line2, line3, line4);
		chart.setSize(600, 450);
		chart.setTitle("Physical Parameters", Color.BLACK, 14);
		chart.setGrid(25, 25, 3, 2);

		// Defining axis info and styles
		AxisStyle axisStyle = AxisStyle.newAxisStyle(Color.BLACK, 12,
				AxisTextAlignment.CENTER);

		AxisLabels xAxis = AxisLabelsFactory.newAxisLabels("Nov", "Dec", "Jan",
				"Feb", "Mar");
		xAxis.setAxisStyle(axisStyle);
		chart.addXAxisLabels(xAxis);

		AxisLabels xAxis2 = AxisLabelsFactory.newAxisLabels("2007", "2007",
				"2008", "2008", "2008");
		xAxis2.setAxisStyle(axisStyle);
		chart.addXAxisLabels(xAxis2);

		AxisLabels xAxis3 = AxisLabelsFactory.newAxisLabels("Month", 50.0);
		xAxis3.setAxisStyle(axisStyle);
		chart.addXAxisLabels(xAxis3);

		AxisLabels yAxis = AxisLabelsFactory.newAxisLabels("", "25", "50",
				"75", "100");
		yAxis.setAxisStyle(axisStyle);
		chart.addYAxisLabels(yAxis);

		return chart;
	}

	@RequestMapping(value = "sensorWeb/measurement", method = RequestMethod.GET)
	public void measurement(@RequestBody String data) {
		logger.info("Entering SensorWebController.measurement()");

		try {
			data = java.net.URLDecoder.decode(data, "UTF-8");
			data = data.split("=")[1];
			String[] arrData = data.split(",");

			Measurement measurement = new Measurement();
			measurement.setTemperature(Float.parseFloat(arrData[0]));
			measurement.setLight(Float.parseFloat(arrData[1]));
			measurement.setSound(Float.parseFloat(arrData[2]));
			measurement.setPressure(Integer.parseInt(arrData[3]));
			measurement.setTimestamp(new Timestamp(System.currentTimeMillis()));
			measurementDAO.save(measurement);

		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}

		logger.info("Exiting HomeController.measurement()");
	}

	@RequestMapping(value = "sensorWeb/plot", method = RequestMethod.GET)
	public @ResponseBody String plot() {
		logger.info("Entering SensorWebController.plot()");
		String data = "";

		Measurement measurement = measurementDAO.getLatestPlot();

		data = measurement.getTimestamp().toString() + ","
				+ measurement.getTemperature().toString() + ","
				+ measurement.getLight().toString() + ","
				+ measurement.getSound().toString() + ","
				+ measurement.getPressure().toString();

		logger.info("Exiting SensorWebController.plot()");
		return data;
	}
}
