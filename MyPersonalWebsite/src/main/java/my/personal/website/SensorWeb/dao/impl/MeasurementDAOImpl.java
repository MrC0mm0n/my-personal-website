package my.personal.website.SensorWeb.dao.impl;

import my.personal.website.SensorWeb.dao.MeasurementDAO;
import my.personal.website.SensorWeb.model.Measurement;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MeasurementDAOImpl implements MeasurementDAO {

	@Autowired
	public SessionFactory sessionFactory;

	@Override
	public void save(Measurement measurement) {
		sessionFactory.getCurrentSession().save(measurement);
	}

	@Override
	public Measurement getLatestPlot() {
		Session session = sessionFactory.getCurrentSession();

		Criteria criteria = session.createCriteria(Measurement.class);
		criteria.addOrder(Order.desc("id"));
		criteria.setMaxResults(1);
		Measurement measurement = (Measurement) criteria.uniqueResult();

		return measurement;
	}

}
