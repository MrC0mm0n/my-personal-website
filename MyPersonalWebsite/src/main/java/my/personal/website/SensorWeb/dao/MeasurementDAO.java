package my.personal.website.SensorWeb.dao;

import my.personal.website.SensorWeb.model.Measurement;

import org.springframework.transaction.annotation.Transactional;

public interface MeasurementDAO {

	@Transactional
	public void save(Measurement measurement);
	
	@Transactional
	public Measurement getLatestPlot();

}
