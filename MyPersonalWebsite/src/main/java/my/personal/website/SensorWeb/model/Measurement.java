package my.personal.website.SensorWeb.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "MEASUREMENT")
public class Measurement {

	@Id
	@GeneratedValue
	@Column(name = "ID")
	Integer Id;

	@Column(name = "TEMPERATURE")
	Float temperature;

	@Column(name = "LIGHT")
	Float light;

	@Column(name = "SOUND")
	Float sound;

	@Column(name = "PRESSURE")
	Integer pressure;

	@Column(name = "TIMESTAMP")
	Timestamp timestamp;

	public Integer getId() {
		return Id;
	}

	public void setId(Integer id) {
		Id = id;
	}

	public Float getTemperature() {
		return temperature;
	}

	public void setTemperature(Float temperature) {
		this.temperature = temperature;
	}

	public Float getLight() {
		return light;
	}

	public void setLight(Float light) {
		this.light = light;
	}

	public Float getSound() {
		return sound;
	}

	public void setSound(Float sound) {
		this.sound = sound;
	}

	public Integer getPressure() {
		return pressure;
	}

	public void setPressure(Integer pressure) {
		this.pressure = pressure;
	}

	public Timestamp getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Timestamp timestamp) {
		this.timestamp = timestamp;
	}

}
