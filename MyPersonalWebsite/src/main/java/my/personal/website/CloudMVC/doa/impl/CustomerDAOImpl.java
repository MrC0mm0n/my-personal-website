package my.personal.website.CloudMVC.doa.impl;

import java.util.List;

import my.personal.website.CloudMVC.doa.CustomerDAO;
import my.personal.website.CloudMVC.model.Customer;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CustomerDAOImpl implements CustomerDAO {

	@Autowired
	private SessionFactory sessionFactory;

	// Create
	public void insertCustomer(Customer customer) {
		Session session = sessionFactory.openSession();
		session.save(customer);
		session.close();
	}

	// Read
	public Customer getById(int id) {
		Session session = sessionFactory.openSession();
		Criteria criteria = session.createCriteria(Customer.class);
		criteria.add(Restrictions.eq("id", id));
		Customer customer = (Customer) criteria.list().get(0);
		session.close();
		return customer;
	}

	@SuppressWarnings("unchecked")
	public List<Customer> getCustomers() {
		Session session = sessionFactory.openSession();
		Criteria criteria = session.createCriteria(Customer.class);
		List<Customer> listCustomer = criteria.list();
		session.close();
		return listCustomer;
	}

	public boolean isCustomerExist(Customer customer) {
		boolean flag = false;
		Session session = sessionFactory.openSession();
		Criteria criteria = session.createCriteria(Customer.class);
		criteria.add(Restrictions.eq("id", customer.getId()));
		if (criteria.list().size() != 0)
			flag = true;
		session.close();
		return flag;
	}

	// Update
	public void updateCustomer(Customer customer) {
		Session session = sessionFactory.openSession();
		Customer existCustomer = (Customer) session
				.createCriteria(Customer.class)
				.add(Restrictions.eq("id", customer.getId())).list().get(0);
		session.getTransaction().begin();
		existCustomer.setCustomer_address(customer.getCustomer_address());
		existCustomer.setCustomer_name(customer.getCustomer_name());
		existCustomer.setCustomer_id(customer.getCustomer_id());
		session.getTransaction().commit();
		session.close();
	}

	// Delete
	public void deleteCustomer(int id) {
		Session session = sessionFactory.openSession();
		Criteria criteria = session.createCriteria(Customer.class);
		criteria.add(Restrictions.eq("id", id));
		session.getTransaction().begin();
		session.delete((Customer) criteria.list().get(0));
		session.getTransaction().commit();
		session.close();
	}

}
