package my.personal.website.CloudMVC.doa.impl;

import java.util.List;

import my.personal.website.CloudMVC.doa.CustomerDAO;
import my.personal.website.CloudMVC.doa.SyncDB2;
import my.personal.website.CloudMVC.model.Customer;
import my.personal.website.CloudMVC.model.CustomerDB2;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SyncDB2Impl implements SyncDB2 {

	@Autowired
	CustomerDAO customerDAO;

	@Autowired
	private SessionFactory sessionFactoryDB2;

	public void deleteAll() {
		Session sessionDB2 = sessionFactoryDB2.openSession();
		List<CustomerDB2> listCustomer = getCustomers();
		if (!listCustomer.isEmpty()) {
			sessionDB2.getTransaction().begin();
			for (CustomerDB2 customer : listCustomer) {
				Criteria criteria = sessionDB2
						.createCriteria(CustomerDB2.class);
				criteria.add(Restrictions.eq("id", customer.getId()));
				sessionDB2.delete((CustomerDB2) criteria.list().get(0));
			}
			sessionDB2.getTransaction().commit();
		}
		sessionDB2.close();
	}

	public void copyAll() {
		List<Customer> listCustomer = customerDAO.getCustomers();
		Session sessionDB2 = sessionFactoryDB2.openSession();
		sessionDB2.getTransaction().begin();
		for (Customer customer : listCustomer) {
			CustomerDB2 customerDB2 = new CustomerDB2();
			customerDB2.setId(customer.getId());
			customerDB2.setCustomer_id(customer.getCustomer_id());
			customerDB2.setCustomer_name(customer.getCustomer_name());
			customerDB2.setCustomer_address(customer.getCustomer_address());
			sessionDB2.save(customerDB2);
		}
		sessionDB2.getTransaction().commit();
		sessionDB2.close();
	}

	@SuppressWarnings("unchecked")
	public List<CustomerDB2> getCustomers() {
		Session sessionDB2 = sessionFactoryDB2.openSession();
		Criteria criteria = sessionDB2.createCriteria(CustomerDB2.class);
		List<CustomerDB2> listCustomer = criteria.list();
		sessionDB2.close();
		return listCustomer;
	}

}
