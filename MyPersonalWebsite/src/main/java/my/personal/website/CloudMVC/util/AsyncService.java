package my.personal.website.CloudMVC.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Service;

//Documentation:
//http://docs.spring.io/spring/docs/3.2.x/spring-framework-reference/html/scheduling.html
//http://www.baeldung.com/spring-async

//Using <task:annotation-driven /> in the application-context will scan the
//entire application for @Async and @Scheduled methods. But using
//@EnableAsync and @EnableScheduling on required classes will configure the
//targeted classes for Async and Scheduled methods

@Service
@EnableAsync
public class AsyncService {
	private static final Logger logger = LoggerFactory
			.getLogger(AsyncService.class);

	@Async
	public void asyncMethod() {
		logger.info("Entering AsyncService.asyncMethod()");
		try {
			for (int i = 1; i < 5; i++) {
				logger.info("Pause: " + i);
				Thread.sleep(1000);
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		logger.info("Exiting AsyncService.asyncMethod()");
	}

}
