package my.personal.website.CloudMVC;

import my.personal.website.CloudMVC.doa.CustomerDAO;
import my.personal.website.CloudMVC.doa.SyncDB2;
import my.personal.website.CloudMVC.model.Customer;
import my.personal.website.CloudMVC.util.AsyncService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/cloudmvc")
public class CloudMVCController {
	private static final Logger logger = LoggerFactory
			.getLogger(CloudMVCController.class);

	@Autowired
	CustomerDAO customerDAO;

	@Autowired
	SyncDB2 syncDB2;

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public ModelAndView home() {
		logger.info("Entering CloudMVCController.home()");

		ModelAndView modelView = new ModelAndView("cloudmvc/cloudmvc");
		modelView.addObject("customerInfo", new Customer());
		modelView.addObject("customerList", customerDAO.getCustomers());
		modelView.addObject("customerListDB2", syncDB2.getCustomers());

		logger.info("Exiting CloudMVCController.home()");
		return modelView;
	}

	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public ModelAndView save(Customer customer) {
		logger.info("Entering CloudMVCController.save()");

		if (customerDAO.isCustomerExist(customer))
			customerDAO.updateCustomer(customer);
		else
			customerDAO.insertCustomer(customer);

		ModelAndView modelView = new ModelAndView("cloudmvc/cloudmvc");
		modelView.addObject("customerInfo", new Customer());
		modelView.addObject("customerList", customerDAO.getCustomers());
		modelView.addObject("customerListDB2", syncDB2.getCustomers());

		logger.info("Exiting CloudMVCController.save()");
		return modelView;
	}

	@RequestMapping(value = "/update", method = RequestMethod.GET)
	public ModelAndView update(@RequestParam(value = "id") int id) {
		logger.info("Entering CloudMVCController.update()");

		ModelAndView modelView = new ModelAndView("cloudmvc/cloudmvc");
		modelView.addObject("customerInfo", customerDAO.getById(id));
		modelView.addObject("customerList", customerDAO.getCustomers());
		modelView.addObject("customerListDB2", syncDB2.getCustomers());

		logger.info("Exiting CloudMVCController.update()");
		return modelView;
	}

	@RequestMapping(value = "/delete", method = RequestMethod.GET)
	public ModelAndView delete(@RequestParam(value = "id") int id) {
		logger.info("Entering CloudMVCController.delete()");

		customerDAO.deleteCustomer(id);

		ModelAndView modelView = new ModelAndView("cloudmvc/cloudmvc");
		modelView.addObject("customerInfo", new Customer());
		modelView.addObject("customerList", customerDAO.getCustomers());
		modelView.addObject("customerListDB2", syncDB2.getCustomers());

		logger.info("Exiting CloudMVCController.delete()");
		return modelView;
	}

	@RequestMapping(value = "/syncdb2", method = RequestMethod.POST)
	public ModelAndView syncDB2() {
		logger.info("Entering CloudMVCController.syncDB2()");

		syncDB2.deleteAll();
		syncDB2.copyAll();

		ModelAndView modelView = new ModelAndView("cloudmvc/cloudmvc");
		modelView.addObject("customerInfo", new Customer());
		modelView.addObject("customerList", customerDAO.getCustomers());
		modelView.addObject("customerListDB2", syncDB2.getCustomers());

		logger.info("Exiting CloudMVCController.syncDB2()");
		return modelView;
	}

	// Ajax method
	@RequestMapping(value = "/util")
	public @ResponseBody String util() {
		logger.info("Entering CloudMVCController.util()");

		logger.info("Exiting CloudMVCController.util()");

		return "If you see this, util method reached(on the server) via jquery ajax call";
	}

	@Autowired
	AsyncService asyncService;

	// Async method
	@RequestMapping(value = "/asyncMethod")
	public ModelAndView asyncMethod() {
		logger.info("Entering CloudMVCController.asyncMethod()");

		try {
			for (int i = 1; i < 3; i++) {
				logger.info("Pause: " + i);
				Thread.sleep(1000);
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		asyncService.asyncMethod();

		ModelAndView modelView = new ModelAndView("cloudmvc/cloudmvc");
		modelView.addObject("customerInfo", new Customer());
		modelView.addObject("customerList", customerDAO.getCustomers());
		modelView.addObject("customerListDB2", syncDB2.getCustomers());

		logger.info("Exiting CloudMVCController.asyncMethod()");
		return modelView;
	}

}
