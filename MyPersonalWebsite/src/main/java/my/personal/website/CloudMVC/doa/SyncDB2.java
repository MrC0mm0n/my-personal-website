package my.personal.website.CloudMVC.doa;

import java.util.List;

import javax.transaction.Transactional;

import my.personal.website.CloudMVC.model.CustomerDB2;

public interface SyncDB2 {

	@Transactional
	void deleteAll();

	@Transactional
	void copyAll();

	@Transactional
	List<CustomerDB2> getCustomers();

}
