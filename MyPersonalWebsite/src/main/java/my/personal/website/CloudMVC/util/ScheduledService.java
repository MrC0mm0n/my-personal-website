package my.personal.website.CloudMVC.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

//Documentation:
// http://docs.spring.io/spring/docs/3.2.x/spring-framework-reference/html/scheduling.html
// http://www.baeldung.com/spring-async

// Using <task:annotation-driven /> in the application-context will scan the
// entire application for @Async and @Scheduled methods. But using
// @EnableAsync and @EnableScheduling on required classes will configure the
// targeted classes for Async and Scheduled methods

@Service
// @EnableScheduling
public class ScheduledService {
	private static final Logger logger = LoggerFactory
			.getLogger(ScheduledService.class);

	@Autowired
	AsyncService asyncService;

	@Scheduled(fixedRate = 7000)
	public void scheduledMethod() {
		logger.info("Entering ScheduledService.scheduledMethod");

		try {
			for (int i = 1; i < 2; i++) {
				logger.info("Pause: " + i);
				Thread.sleep(1000);
				asyncService.asyncMethod();
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		logger.info("Exiting ScheduledService.scheduledMethod");
	}

}
