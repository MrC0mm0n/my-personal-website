package my.personal.website.CloudMVC.doa;

import java.util.List;

import javax.transaction.Transactional;

import my.personal.website.CloudMVC.model.Customer;

public interface CustomerDAO {

	// Create
	@Transactional
	void insertCustomer(Customer customer);

	// Read
	@Transactional
	Customer getById(int customerId);

	@Transactional
	List<Customer> getCustomers();

	@Transactional
	boolean isCustomerExist(Customer customer);

	// Update
	@Transactional
	void updateCustomer(Customer customer);

	// Delete
	@Transactional
	void deleteCustomer(int id);

}
