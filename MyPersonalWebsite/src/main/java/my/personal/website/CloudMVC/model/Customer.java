package my.personal.website.CloudMVC.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.search.annotations.Analyze;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Index;
import org.hibernate.search.annotations.Indexed;

@Entity
@Indexed
@Table(name = "CUSTOMER")
public class Customer {

	@Id
	@GeneratedValue
	@Column(name = "ID")
	int id;

	@Field(index = Index.YES, analyze = Analyze.YES)
	@Column(name = "CUSTOMER_ID", unique = true, nullable = false)
	long customer_id;

	@Column(name = "CUSTOMER_NAME", nullable = false)
	String customer_name;

	@Column(name = "CUSTOMER_ADDRESS", nullable = false)
	String customer_address;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public long getCustomer_id() {
		return customer_id;
	}

	public void setCustomer_id(long customer_id) {
		this.customer_id = customer_id;
	}

	public String getCustomer_name() {
		return customer_name;
	}

	public void setCustomer_name(String customer_name) {
		this.customer_name = customer_name;
	}

	public String getCustomer_address() {
		return customer_address;
	}

	public void setCustomer_address(String customer_address) {
		this.customer_address = customer_address;
	}

}
